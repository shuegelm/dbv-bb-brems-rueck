package logic;

import ij.IJ;
import ij.ImagePlus;
import ij.process.ImageProcessor;
import javafx.geometry.BoundingBox;
import model.CustomBoundingBox;

import java.util.ArrayList;

/**
 * Created by Sebastian Huegelmann and Jan-Erik Gerstberger on 23.06.2017.
 */
public class FindBlinker {
    /**
     * Sucht in der übergebenen BoundingBox innerhalb des Bildes nach einem Blinklicht und gibt eine BoundingBox dieses zurück.
     * @param img
     * @param bBox
     * @return new BoundingBox
     */
    public static CustomBoundingBox findBlinker(ImagePlus img, BoundingBox bBox) {

        ImageProcessor ip = img.getProcessor();
        ImagePlus imgDuplicate = img.duplicate();
        ImageProcessor ipDraw = imgDuplicate.getProcessor();
        int[] color = new int[3];
        ArrayList<Integer> xArray = new ArrayList<Integer>();
        ArrayList<Integer> yArray = new ArrayList<Integer>();

        //IJ.run(img, "Enhance Contrast", "70");
        for (int y = (int) bBox.getMinY(); y < bBox.getMinY()+ bBox.getHeight();y++) {
            for (int x = (int) bBox.getMinX(); x < bBox.getMinX()+ bBox.getWidth();x++) {
                color = ip.getPixel(x,y,null);
                if ((color[0] >= 240) && (color[1] >= 240) && (color[2] <= 160)) {
                    xArray.add(x);
                    yArray.add(y);
                    ipDraw.putPixel(x,y,new int[]{0,255,0});
                }
            }
        }

        for (int i = 0; i < xArray.size(); i++) {
            ArrayList<int[]> nachbarn = new ArrayList<int[]>();
            int[] a = {xArray.get(i)+1, yArray.get(i)+1};nachbarn.add(a);
            int[] b = {xArray.get(i)+1, yArray.get(i)};nachbarn.add(b);
            int[] c = {xArray.get(i)+1, yArray.get(i)-1};nachbarn.add(c);
            int[] d = {xArray.get(i)-1, yArray.get(i)+1};nachbarn.add(d);
            int[] e = {xArray.get(i)-1, yArray.get(i)};nachbarn.add(e);
            int[] f = {xArray.get(i)-1, yArray.get(i)-1};nachbarn.add(f);
            int[] g = {xArray.get(i), yArray.get(i)+1};nachbarn.add(g);
            int[] h = {xArray.get(i), yArray.get(i)-1};nachbarn.add(h);

            for (int [] bernd: nachbarn) {
                color = ipDraw.getPixel(bernd[0], bernd[1], null);
                if ((color[0] >= color[2]+50) && (color[1] >= color[2]+50)) {
                    xArray.add(bernd[0]);
                    yArray.add(bernd[1]);
                    ipDraw.putPixel(bernd[0],bernd[1],new int[]{0,255,0});
                }

            }
        }
        //ToDo Array säubern nach hinzufügen

        img.setProcessor(ip);

        if ((xArray.size() > 0) && (yArray.size() > 0)) {
            int minX = xArray.get(0), minY = yArray.get(0), maxX = xArray.get(0), maxY = yArray.get(0);
            for (int j = 0; j < xArray.size(); j++) {
                if (xArray.get(j) <= minX ) {
                    minX = xArray.get(j);
                }
                if (xArray.get(j) >= maxX) {
                    maxX = xArray.get(j);
                }
            }
            for (int k = 0; k < yArray.size(); k++) {
                if (yArray.get(k) <= minY ) {
                    minY = yArray.get(k);
                }
                if (yArray.get(k) >= maxY) {
                    maxY = yArray.get(k);
                }
            }
            //new ImagePlus("Farbig",ip).show();
            return new CustomBoundingBox(new BoundingBox(minX,minY,maxX-minX,maxY-minY), true, 0);
        } else {
            return null;
        }
    }
}
