package logic;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.Line;
import ij.gui.ProfilePlot;
import ij.measure.ResultsTable;
import ij.plugin.filter.ParticleAnalyzer;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import javafx.geometry.BoundingBox;
import model.CustomBoundingBox;
import java.util.ArrayList;

/**
 * Created by Sebastian Huegelmann and Jan-Erik Gerstberger on 23.06.2017.
 */
public class FindBrakeLight {

    public static ArrayList<CustomBoundingBox> findBrakeLight(ImagePlus img, BoundingBox bBox) {
        ImageProcessor ip = img.getProcessor();
        ImagePlus imgDuplicate = img.duplicate();
        ImageProcessor ipDraw = macheWeiss(imgDuplicate);

        //IJ.log("Titel des Bildes: "+ img.getTitle() + "\n");
        int[] color = new int[3];
        ArrayList<Integer> xArray = new ArrayList<Integer>();
        ArrayList<Integer> yArray = new ArrayList<Integer>();
        ArrayList<CustomBoundingBox> boundingBoxes = new ArrayList<CustomBoundingBox>();

        for (int y = (int) bBox.getMinY(); y < bBox.getMinY()+ bBox.getHeight();y++) {
            for (int x = (int) bBox.getMinX(); x < bBox.getMinX()+ bBox.getWidth();x++) {
                color = ip.getPixel(x,y,null);
                if ((color[0] >= color[1]+50) && (color[0] >= color[2]+50)) {
                    xArray.add(x);
                    yArray.add(y);
                    //malt Pink im Originalbild
                    //ip.putPixel(x,y,new int[]{255,0,255});
                    //malt in zweiten IP schwarz
                    ipDraw.putPixel(x,y,new int[]{0,0,0});
                }
            }
        }

        ResultsTable rt = new ResultsTable();
        ParticleAnalyzer particleAnalyzer = new ParticleAnalyzer(0,1+2+4+8+16+32+64+128+256+512,rt,0,Integer.MAX_VALUE); //ParticleAnalyzer.SHOW_OUTLINES+ParticleAnalyzer.SHOW_RESULTS
        particleAnalyzer.analyze(imgDuplicate);

        if (rt.getCounter() > 0) {
            float[] area = rt.getColumn(0);
            float[] xm = rt.getColumn(8);
            float[] ym = rt.getColumn(9);
            float[] bx = rt.getColumn(11);
            float[] by = rt.getColumn(12);
            float[] widthR = rt.getColumn(13);
            float[] heightR = rt.getColumn(14);

            //finde 3 größten Areas
            int max1 = 0, max2 = 0, max3 = 0, leftLight=0, rightLight=0;
            for (int i= 0; i < rt.getCounter(); i++) {
                if (area[i] >= area[max1]) {
                    max3 = max2;
                    max2 = max1;
                    max1 = i;
                } else if (area[i] > area[max2]) {
                    max3 = max2;
                    max2 = i;
                } else if (area[i] > area[max3]) {
                    max3 = i;
                }
            }

            //schauen, ob die 2  Areas ca. auf einer Höhe liegen
            double distance = (int)(heightR[max2])*1.5;
            if (!((ym[max1] <= ym[max2]+distance)&&(ym[max1] >= ym[max2]-distance))) {
                return null;
            }

            //determine which light is the left one
            if (bx[max1] < bx[max2]) {
                leftLight = max1;
                rightLight = max2;
            } else {
                leftLight = max2;
                rightLight = max1;
            }

            //müssen 2 distinkte Regionen sein
            if (max1 == max2) {
                return null;
            }

            findLights(img, boundingBoxes, area[leftLight], new Line(bx[leftLight], ym[leftLight], bx[leftLight] + widthR[leftLight], ym[leftLight]), bx[leftLight], (by[leftLight] + (heightR[leftLight] / 2)), true);
            findLights(img, boundingBoxes, area[rightLight], new Line(bx[rightLight], ym[rightLight], bx[rightLight] + widthR[rightLight], ym[rightLight]), bx[rightLight], (by[rightLight] + (heightR[rightLight] / 2)), false);
        }

        //imgDuplicate.show();

        if (boundingBoxes.size() > 0) {
            return boundingBoxes;
        } else {
            return null;
        }
    }

    private static void findLights(ImagePlus img, ArrayList<CustomBoundingBox> boundingBoxes, float v, Line line1, float bx, float v1, boolean isLeft) {
        Line line = line1;
        img.setRoi(line);
        ProfilePlot plot = new ProfilePlot(img);
        //plot.createWindow();


        double[] profile = plot.getProfile();
        int start = (profile.length * 4) / 100;
        int zone = (profile.length - start - start)/4;
        int[] mittelPunkt = new int[4];

        //IJ.log("Die Area hat die Nummer"+ v + " und hat den MaxWert =" + plot.getMax() + "\n");

        for (int i = 0; i < 4; i++) {
            int width = 0, type = i;
            double median = 0;
            boolean status = false;
            for (int k = start + (zone * i); k < start + (zone * (i + 1)); k++) {
                median = median + profile[k];
            }
            //IJ.log("Zone" + i + " hat den Median mit dem Helligkeitswert " + (median / zone) + "\n");
            mittelPunkt[i] = (int) bx +start+(zone/2)+(i*zone);
            if (i == 4) {
                width = zone-start;
            } else {
                width = zone;
            }
            //schauen ob das licht an ist
            if ((median / zone) > 120) {
                status = true;
            }
            if (!isLeft) {
                if (i == 0) type=3;
                if (i == 1) type=2;
                if (i == 2) type=1;
                if (i == 3) type=0;
            }
            boundingBoxes.add(new CustomBoundingBox(new BoundingBox(mittelPunkt[i]-((zone-start-start)/2), v1 -((zone-start-start)/2),zone,width), status, type));
        }
    }

    private static ImageProcessor macheWeiss(ImagePlus img) {
        ImageConverter ic = new ImageConverter(img);
        ic.convertToGray8();
        ImageProcessor ip = img.getProcessor();
        for (int x = 0; x < ip.getWidth(); x++) {
            for (int y = 0; y < ip.getHeight(); y++) {
                ip.putPixel(x,y,new int[]{255,255,255});
            }
        }
        return ip;
    }

}
