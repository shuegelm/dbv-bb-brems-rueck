package model;

import javafx.geometry.BoundingBox;

/**
 * Created by Jan-Erik status 20.06.2017.
 */

public class CustomBoundingBox {

    BoundingBox boundingBox = new BoundingBox(0, 0, 0 ,0);
    boolean status = false; //an = true, aus = false
    int type = 0; //0= Blinker, 1= Ruecklicht, 2= Bremslicht, 3=rueckfahrlicht

    public CustomBoundingBox(BoundingBox bb, boolean status, int type) {
        this.boundingBox = bb;
        this.status = status;
        this.type = type;
    }

    public BoundingBox getBoundingBox() {
        return boundingBox;
    }

    public void setBoundingBox(BoundingBox boundingBox) {
        this.boundingBox = boundingBox;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
