import ij.IJ;
import ij.ImagePlus;
import ij.Macro;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.TextRoi;
import ij.plugin.PlugIn;
import javafx.geometry.BoundingBox;
import logic.FindBrakeLight;
import logic.FindBlinker;
import model.CustomBoundingBox;
import java.awt.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class main implements PlugIn{

    private static final ArrayList<String> names = new ArrayList<String>();
    private static final ArrayList<BoundingBox[]> boundingBoxes = new ArrayList<BoundingBox[]>();
    private static final String TEST_STRING = "C:\\input.txt";

    String path;

    public void run(String s) {

        if (IJ.isMacro() && Macro.getOptions() != null && !Macro.getOptions().trim().isEmpty()) {
            // get the arguments, put separator in trim if passed multiple arguments, e.g. '100, 20'
            String args = Macro.getOptions().trim();
            path = args;
            loadFile(path);
        } else {
            loadFile(TEST_STRING);
        }

        for (int i=0; i< names.size(); i++) {
            ImagePlus img = IJ.openImage(names.get(i));
            img = processImg(img, boundingBoxes.get(i));
            img.show();
        }
    }

    private static  ImagePlus processImg(ImagePlus img, BoundingBox[] bbArray) {

        Overlay overlay = new Overlay();

        for (BoundingBox b : bbArray) {
            overlay.add(drawRect(b, Color.GREEN, 5));
            CustomBoundingBox box = FindBlinker.findBlinker(img,b);
            ArrayList<CustomBoundingBox> rueckLichter = FindBrakeLight.findBrakeLight(img,b);
            if (rueckLichter != null) {
                for (CustomBoundingBox cb: rueckLichter) {
                    overlay = drawRect(cb, overlay);
                }
            }
            if (box != null) {
                overlay = drawRect(box, overlay);
            }
        }
        img.setOverlay(overlay);

        return img;
    }

    public static Overlay drawRect(CustomBoundingBox box, Overlay overlay) {
        Color color;
        if (box.isStatus()) {
            color = Color.GREEN;
        } else {
            color = Color.RED;
        }
        BoundingBox bBox = box.getBoundingBox();
        Rectangle rect1 = new Rectangle((int)bBox.getMinX(), (int)bBox.getMinY(), (int)bBox.getWidth(), (int)bBox.getHeight());
        Roi roi = new Roi(rect1);
        roi.setStrokeColor(color);
        roi.setStrokeWidth(0.5);
        overlay.add(roi);

        overlay.add(new TextRoi(bBox.getMinX(), bBox.getMinY()+10, ""+box.getType()));

        return overlay;
    }

    public static Roi drawRect(BoundingBox box, Color color, int width) {
        Rectangle rect1 = new Rectangle((int)box.getMinX(), (int)box.getMinY(), (int)box.getWidth(), (int)box.getHeight());
        Roi roi = new Roi(rect1);
        roi.setStrokeColor(color);
        roi.setStrokeWidth(width);
        return roi;
    }

    private static void loadFile(String fileName) {
        //IJ.log(fileName+"\n");
        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);
            String line = br.readLine();

            while (line !=null) {
                String[] zeile = line.split(";");
                //IJ.log(""+zeile[0]+"\n");
                names.add(zeile[0]);
                BoundingBox[] tempBBarray = new BoundingBox[zeile.length-1];
                for (int i = 1; i < zeile.length; i++) {
                    String[] koords = zeile[i].split(",");
                    tempBBarray[i-1] = new BoundingBox(Double.parseDouble(koords[0]),Double.parseDouble(koords[1]),Double.parseDouble(koords[2]),Double.parseDouble(koords[3]));
                    //IJ.log(""+koords[0]+"\n");
                }
                boundingBoxes.add(tempBBarray);
                line = br.readLine();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            //IJ.log(""+e);
        } catch (IOException e) {
            e.printStackTrace();
            //IJ.log(""+e);
        }

    }


}