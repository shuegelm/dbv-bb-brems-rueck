# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
DBV Admission
* Version
0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Just fucking clone it.
* Configuration
* Dependencies

```
#!xml

    <dependencies>
        <dependency>
            <groupId>gov.nih.imagej</groupId>
            <artifactId>imagej</artifactId>
            <version>1.47</version>
        </dependency>
    </dependencies>
```

* Deployment instructions
Create maven project

### Maven pom.xml ###

```
#!xml

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>imagejplugins</groupId>
    <artifactId>imagejplugins_</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <packaging>jar</packaging>
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    </properties>
    <dependencies>
        <dependency>
            <groupId>gov.nih.imagej</groupId>
            <artifactId>imagej</artifactId>
            <version>1.47</version>
        </dependency>
    </dependencies>
</project>
```


### What to do? ###
Als Erstes suchen wir nach den Brems- bzw. Blinklichtern. Dies können wir evtl über die Farbe tun. Bremslichter sind rot, auch wenn sie aus sind (dunkelrot). Blinklichter sind gelb, dunkelgelb wenn diese aus sind.
Ob diese an oder aus sind, erkennt man an den umliegenden Pixeln, welche Helligkeit diese aufweisen (Richtung weiß).
Die Bounding Boxen werden dann mit einen Overlay drüber gelegt. Vllt Region und dann nur Outline verwenden. Um Konturen zu finden kann man z.B. 4er, 8er Nachbarschaft?

8 Detektion von Blink- und Bremslichtern in Bounding-Boxen
Eingabe: Bild (RGB) mit Liste von Bounding-Boxen
Ausgabe: Liste von (Bounding-Box, Position(en), blink / brems, aus / an) für jedes erkannte
Bremslicht
Anzeige: Bounding-Boxen mit erkanntem Zustand im Overlay